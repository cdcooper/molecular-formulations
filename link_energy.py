import PBL, PBL_stern
import numpy as np

dens = np.array([ 2., 4., 8., 16., 32. ])

link_energy_l = []
link_energy_s = []

for dd in dens:
	energy_p = PBL.solvation_energy('peptide2', dd, info=True)
	energy_r = PBL.solvation_energy('rna', dd, info=True)

	energy_c = PBL.solvation_energy('complex', dd, info=True)

	link_energy_l.append(energy_c - (energy_p + energy_r))

for dd in dens:
	energy_p = PBL_stern.solvation_energy('peptide2', dd, info=True)
	energy_r = PBL_stern.solvation_energy('rna', dd, info=True)

	energy_c = PBL_stern.solvation_energy('complex', dd, info=True)

	link_energy_s.append(energy_c - (energy_p + energy_r))

print link_energy_l
print link_energy_s
