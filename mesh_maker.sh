#!/bin/bash

pqr_file="my_pqr/$1.pqr"
xyzr_file="nanoShaper/$1.xyzr"

echo $2

#awk '{print $7, $8, $9, $11}' $pqr_file > $xyzr_file
awk '{print $6, $7, $8, $10}' $pqr_file > $xyzr_file
sed -i '/^\s*$/d' $xyzr_file

cd nanoShaper/
sed "s/test.xyzr/$1.xyzr/g" sConf.prm > surfaceConfiguration.prm
sed -i "s/Grid_scale = 1.0/Grid_scale = $2/g" surfaceConfiguration.prm
./NanoShaper

mkdir -p ../geometries/$1

mv triangulatedSurf.face ../geometries/$1/$1.face
mv triangulatedSurf.vert ../geometries/$1/$1.vert

rm *txt *xyz *xyzr
