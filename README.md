# Molecular-Formulations
Code to calculate the electrostatic potential and dissolution energy in a molecule immerse in a dielectric medium. Solved by a Boundary Element Method and implemented in Python and BEM++.

## main.py 
main program used to run series of PBL solvers
## PBL.py
PBL (PoissonBoltzmann-Laplace) function used to calculate the solvation energy of a molecule
## PBL_stern.py
PBL function used to calculate the solvation energy adding a Stern layer
## general_functions.py
Set of common functions used in the PBLs programs
## mesh_maker
Creates .msh files from .vert & .face files (this files must be in a geometry/ directory in the molecule directory)
## link_energy
Program used to calculate the differential energy between a molecule array and the individual molecules

